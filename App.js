import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { createStackNavigator, createBottomTabNavigator, createAppContainer } from 'react-navigation';
import PopularScreen from './pages/PopularScreen';
import UpcomingScreen from './pages/UpcomingScreen';
import NowPlayingScreen from './pages/NowPlaying';
import DetailsScreen from './pages/DetailScreen';

const PopularStack = createStackNavigator(
  {
    Popular: { screen: PopularScreen },
    Details: { screen: DetailsScreen },
  },
  {
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#303f9f',
      },
      headerTintColor: '#FFFFFF',
      title: 'Popular',
    },
  }
);

const UpcomingStack = createStackNavigator(
  {
    Upcoming: { screen: UpcomingScreen },
    Details: { screen: DetailsScreen },
  },
  {
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#303f9f',
      },
      headerTintColor: '#FFFFFF',
      title: 'Upcoming',
    },
  }
);

const NowPlayingStack = createStackNavigator(
  {
    NowPlaying: { screen: NowPlayingScreen },
    Details: { screen: DetailsScreen },
  },
  {
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#303f9f',
      },
      headerTintColor: '#FFFFFF',
      title: 'Now Playing',
    },
  }
);

const App = createBottomTabNavigator(
  {
    NowPlaying: { screen: NowPlayingStack },
    Popular: { screen: PopularStack },
    Upcoming: { screen: UpcomingStack },
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        let IconComponent = Icon;
        let iconName;
        if (routeName === 'NowPlaying') {
          iconName = `play-circle${focused ? '-filled' : '-outline'}`;
        } else if (routeName === 'Popular') {
          iconName = `star${focused ? '' : '-border'}`;
        } else if (routeName === 'Upcoming') {
          iconName = `add-circle${focused ? '' : '-outline'}`;
        }
        return <IconComponent name={iconName} size={25} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: '#303f9f',
      inactiveTintColor: 'gray',
    },
  }
);

export default createAppContainer(App);