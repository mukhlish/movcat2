import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  ImageBackground,
  TouchableOpacity,
  Text
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';
import { blue } from 'ansi-colors';

export default class movieDetail extends Component {
  render() {
    return (
      <View>
        <ImageBackground source={{ uri: 'https://image.tmdb.org/t/p/w1400_and_h450_face/8bRIfPGDnmWgdy65LO8xtdcFmFP.jpg' }} style={{ height: 250 }}>
          <View style={styles.poster} />
        </ImageBackground>
        <View style={styles.container}>
          <View style={styles.descContainer}>
            <TouchableOpacity>
              <Image source={{ uri: 'https://randomuser.me/api/portraits/thumb/men/0.jpg' }} style={{ width: 100, height: 100, borderRadius: 50 }} />
            </TouchableOpacity>
            <View style={styles.movieDetails}>
              <Text numberOfLines={2} style={styles.movieTitle}>Spiderman : Homecoming</Text>
              <Text style={styles.movieStats}>31 August 2019</Text>
              <View style={styles.rating}>
                <Image resizeMode='contain' source={{ uri: 'https://cdn3.iconfinder.com/data/icons/flat-actions-icons-9/792/Star_Gold-512.png' }} style={{ width: 20, height: 20 }} />
                <Text>Rating: 7.2</Text>
              </View>
            </View>
          </View>
          <View style={styles.movieDesc}>
            <Text style={styles.descTitle}>Overview</Text>
            <Text style={styles.description}>Miles Morales is juggling his life between being a high school student and being a spider-man. When Wilson "Kingpin" Fisk uses a super collider, others from across the Spider-Verse are transported to this dimension</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 15,
  },
  descContainer: {
    flexDirection: 'row',
    paddingTop: 15,
  },
  movieDetails: {
    paddingHorizontal: 15,
    flex: 1,
  },
  movieTitle: {
    fontSize: 18,
    fontWeight: '500',
    color: '#1c1c1c',
    marginBottom: 10,
  },
  movieStats: {
    fontSize: 14,
    paddingTop: 3,
    color: '#5c5c5c',
    marginBottom: 10,
  },
  poster: {
    backgroundColor: '#000',
    height: 250,
    opacity: 0.75,
  },
  star: {
    width: 50,
  },
  movieDesc: {
    marginTop: 20,
  },
  descTitle: {
    fontSize: 16,
    fontWeight: '500',
    color: '#303f9f',
    marginBottom: 5,
  },
  rating: {
    flexDirection: 'row',
  },
  description: {
    lineHeight: 25,
    textAlign: 'justify',
  }
});