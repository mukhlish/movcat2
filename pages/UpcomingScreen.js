import React from 'react';
import { Text, View, TouchableOpacity, StyleSheet,ScrollView } from 'react-native';
import MovieItem from "./MovieItem";

export default class UpcomingScreen extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <ScrollView showsVerticalScrollIndicator={false} >
        
        <TouchableOpacity onPress={() => this.props.navigation.navigate('Details')}>
          <MovieItem 
            imageUri={'https://m.media-amazon.com/images/M/MV5BYTJlNjlkZTktNjEwOS00NzI5LTlkNDAtZmEwZDFmYmM2MjU2XkEyXkFqcGdeQXVyNjg2NjQwMDQ@._V1_.jpg'}
            judul="IT Chapter Two"
            release="6 September 2019"
            rate="n.a"
            desc="desc about the movie here"
          />
        </TouchableOpacity>
        
        <TouchableOpacity onPress={() => this.props.navigation.navigate('Details')}>
          <MovieItem 
            imageUri={'http://www.gstatic.com/tv/thumb/movieposters/14592194/p14592194_p_v8_aa.jpg'}
            judul="Terminator 6"
            release="23 Oktober 2019"
            rate="n.a"
            desc="desc about the movie here"
          />
        </TouchableOpacity>

        <TouchableOpacity onPress={() => this.props.navigation.navigate('Details')}>
          <MovieItem 
            imageUri={'https://m.media-amazon.com/images/M/MV5BNGVjNWI4ZGUtNzE0MS00YTJmLWE0ZDctN2ZiYTk2YmI3NTYyXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_.jpg'}
            judul="Joker (2019)"
            release="4 Oktober 2019"
            rate="n.a"
            desc="desc about the movie here"
          />
        </TouchableOpacity>
        
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignSelf: 'flex-start',
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
    width: 300,
    marginTop: 16,
  },
});