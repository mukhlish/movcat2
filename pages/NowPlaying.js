import React from 'react';
import { Text, View, TouchableOpacity, StyleSheet,ScrollView } from 'react-native';
import MovieItem from "./MovieItem";

export default class NowPlayingScreen extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <ScrollView showsVerticalScrollIndicator={false} >
        
        <TouchableOpacity onPress={() => this.props.navigation.navigate('Details')}>
          <MovieItem 
            imageUri={'https://cdn.cgv.id/uploads/movie/compressed/19025300.jpg'}
            judul="Ready or Not"
            release="24 Agustus 2019"
            rate="8.2"
            desc="desc about the movie here"
          />
        </TouchableOpacity>
        
        <TouchableOpacity onPress={() => this.props.navigation.navigate('Details')}>
          <MovieItem 
            imageUri={'http://cinemaxx.cinemaxxtheater.com/Gallery/Movies/Thumbnail/Gundala-post.jpg'}
            judul="Gundala"
            release="29 Agustus 2019"
            rate="8.7"
            desc="desc about the movie here"
          />
        </TouchableOpacity>
        
        <TouchableOpacity onPress={() => this.props.navigation.navigate('Details')}>
          <MovieItem 
            imageUri={'https://cdn.cgv.id/uploads/movie/compressed/19022600.jpg'}
            judul="Bumi Manusia"
            release="15 Agustus 2019"
            rate="8.0"
            desc="desc about the movie here"
          />
        </TouchableOpacity>
      
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignSelf: 'flex-start',
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
    width: 300,
    marginTop: 16,
  },
});