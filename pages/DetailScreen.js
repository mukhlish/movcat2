import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import MovieDetail from '../component/MovieDetail';

export default class DetailsScreen extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <MovieDetail />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});