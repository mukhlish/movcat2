import React from 'react';
import { Text, View, TouchableOpacity, StyleSheet,ScrollView } from 'react-native';
import MovieItem from "./MovieItem";

export default class PopularScreen extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <ScrollView showsVerticalScrollIndicator={false} >
        
        <TouchableOpacity onPress={() => this.props.navigation.navigate('Details')}>
          <MovieItem 
            imageUri={'https://upload.wikimedia.org/wikipedia/id/thumb/2/2e/Bohemian_Rhapsody_poster.png/220px-Bohemian_Rhapsody_poster.png'}
            judul="Bohemian Rhapsody"
            release="27 Oktober 2018"
            rate="8.9"
            desc="desc about the movie here"
          />
        </TouchableOpacity>
        
        <TouchableOpacity onPress={() => this.props.navigation.navigate('Details')}>
          <MovieItem 
            imageUri={'https://upload.wikimedia.org/wikipedia/id/thumb/9/90/AStarIsBornBradleyCooperPoster2018.jpg/250px-AStarIsBornBradleyCooperPoster2018.jpg'}
            judul="A Stars Is Born"
            release="19 Oktober 2018"
            rate="8.7"
            desc="desc about the movie here"
          />
        </TouchableOpacity>

        <TouchableOpacity onPress={() => this.props.navigation.navigate('Details')}>
          <MovieItem 
            imageUri={'https://m.media-amazon.com/images/M/MV5BMjAzMTI1MjMyN15BMl5BanBnXkFtZTgwNzU5MTE2NjM@._V1_.jpg'}
            judul="Bird Box"
            release="14 Desember 2018"
            rate="8.2"
            desc="desc about the movie here"
          />
        </TouchableOpacity>

        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignSelf: 'flex-start',
  
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
    width: 300,
    marginTop: 16,
  },
});