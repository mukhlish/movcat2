import React, { Component } from 'react';
import { Text, View, TouchableOpacity, StyleSheet, Image } from 'react-native';

export default class MovieItem extends Component {
  render() {
    return (
        <View style={styles.movieItem1cont}>
            <Image
              source={{uri:this.props.imageUri}}
              style={styles.movieItem1poster}
            />
              <View style= {styles.MovieItem1DescCont}>
                <Text style={styles.movieItem1title}>{this.props.judul}</Text>
                <Text style={styles.movieItem1release}>Release: {this.props.release}</Text>
                <View style={styles.rating}>
                    <Image 
                    source={{uri:'https://png.pngtree.com/svg/20161118/stars_425193.png'}} 
                    style={styles.starsIcon}
                    />
                    <Text style={styles.ratingText}>{this.props.rate}</Text>
                </View>
                <Text style={styles.movieItem1movdescTitle}>Movie Desc:</Text>
                <Text style={styles.movieItem1movdesc}>{this.props.desc}</Text>
              </View>
            </View>
    );
  } 
}


const styles = StyleSheet.create({
    movieItem1cont:{
      flex: 1,
      paddingTop: 15,
      paddingLeft: 10,
      flexDirection: 'row'
    },
    MovieItem1DescCont: {
        flexDirection: 'column',
        paddingTop:10,
        paddingLeft:15,
    },
    movieItem1title:{
        fontSize: 15,
        textAlign: 'left',
    },
    movieItem1release:{
        color: "grey",
        fontSize: 10,
        textAlign: 'left',
        paddingTop: 5,
        
    },
    movieItem1poster:{
        height: 200,
        width: 100,
    },
    rating:{
        flexDirection:'row',
    },
    ratingText:{
        fontSize: 15,
        padding:5,
    },
    starsIcon:{
        width:25,
        height:25,        
    },
    movieItem1movdescTitle:{
        color: "grey",
        fontSize: 15,
        textAlign: 'left',
        paddingTop: 5,
    },
    
    movieItem1movdesc:{
        flexWrap: 'wrap',
        color: "grey",
        fontSize: 15,
        textAlign: 'left',
        flexGrow: 1,
        flex: 10
    },
});